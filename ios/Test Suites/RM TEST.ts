<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM TEST</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7bbcd82f-48d3-40a7-973c-5260069a6520</testSuiteGuid>
   <testCaseLink>
      <guid>9fb2badb-8307-4707-9a04-1d3f8edc3469</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162285 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a82c98d0-ff88-42b4-9fe0-e7dfa51944c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162364 (analize)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd6a2ed5-b502-47dc-a820-6d7018461d09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162258 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8d8c694-307e-4af8-95aa-d7e063d4dd78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162304 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c9568d4-0e5b-4909-a64d-acaae52676bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162248 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b15e045-3542-4b5b-8ea0-c1413ec62a0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162308 (medicii mei)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a8fb67d-d892-4ad8-a787-91accb432c6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162320 (hmburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34827307-593b-4f0f-ae49-58bedd5568e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162300 (hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3ed3bd5-97de-4c4f-b644-db13a10d1eab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162331 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6e57143-232d-4d6c-b34a-5a724e0ff425</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162283 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>711cef71-d68c-452c-9857-f30c557f92c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162284 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42bf9012-8a0e-44cb-98b5-aecadf382d12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162256 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a442962a-a540-4202-aae1-848a56f242f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162267 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d40d7739-4973-4d1c-9338-6ac7d4a4c3fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162251 (BMI)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94cf2527-3dd8-465f-b4cf-79d04e59427d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162336 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1bc68ef-4536-4370-b5e2-64f3c2672177</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162358 (hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2900779d-1caf-40a5-be53-dfd2aab62ad1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162279 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ae93271-2832-4bf1-8416-733d7168c5c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162262 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0461962-7be8-4870-afe6-775465ec5734</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162253 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ddb5de5-bdcd-48cb-9816-4dffccdf5622</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162286 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12486f55-0b66-47b2-9952-851236264ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162257 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10f8adf3-43df-4ee0-b722-f8eec20782e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162371 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b184d6dd-597c-42f5-9059-73987c8849cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162309 (medicii mei)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80d60016-6b2d-428f-8633-2d9d7ba0859c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162297 (analize)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7ca8d68-d359-4ea3-aa58-cb1b77ef1a95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162250 (banca de km)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2317f968-5214-4dcf-b71a-a70d143aafbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162259 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af950a7d-5798-4179-935d-ce1a76b06d7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162276 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d177862-d054-4eee-8dc1-e3e17fd1d8cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162301 (despre)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c90b244e-07c8-49a2-9abc-c69f216edf49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162249 (banca de km)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9221da9-2ee2-4ece-bc59-354728675a3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162255 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c3d64fa-f0e9-42ac-99c7-5730077ed897</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162247 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a9d0140-af54-4a06-9415-fded4a294661</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162275 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>818da5eb-505a-4a4a-8f73-0d9ada1ea598</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162246 (Hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>901b8557-3dc8-47c8-92a8-538765663cbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162355 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05b2f2bd-a43c-4a5d-af15-e5cc45e05292</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162354 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>315f79f4-b60f-447e-a930-4b1a2d7431e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162310 (medicii mei)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f029d16-e558-4c19-9e24-8dcdd5de4299</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162363 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d94a159-cce0-405e-8148-ecb380b1f20b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162357 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89f39ba1-b353-4863-8496-f6db94c616af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162359 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>197827b6-07b7-4eed-a3b2-d84724ccbcf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162265 (dictionar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>191c4671-0b20-4bc1-85bc-b6a62bce2acb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162293 (banca de km) - outdated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b48be1ec-1ec4-499f-9fb6-417f7e810c0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162327 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6601b2dd-3e71-4c19-b9d6-a56ce722de2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162328 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48993eff-9711-495c-bb63-cf90affded93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162366 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a45685d-5846-4806-a9e7-3747b3adaf38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162325 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a245c11b-eeb2-4324-a496-05e328a18efd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162260 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5abfd34-f5f8-480b-864c-83eb755272b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162327 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40433c3e-1270-4b66-90d2-0b5dfc47e484</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162353 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51035752-e05b-487f-a01c-5cfb34bf9778</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162254 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3be924b0-d744-4f81-9963-394c2a09e1ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162263 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0abc1904-243a-495b-850a-d4eaca3dfab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162274 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c89d775a-141b-45f9-b028-2ac29be3f549</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162288 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bf35172-abc5-404a-becd-4575f5474d77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162337 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b8b4825-9000-43be-a816-891586b526ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162273 (statistici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c171f705-6fa9-43ef-881b-906c6648b312</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162361 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97ca4268-938c-4b0d-bcd8-b0b6afb4ef25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162362 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78aa6cbe-4491-4720-97bb-a5a02a2ac840</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162303 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d7e2c56-45a5-4daa-b2e4-b94a5f279bf7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162321 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c7fa61a-9a1f-4259-89bf-67226489de9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM MobApp Regression Tests/Pacient - T162252 (profil user)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
