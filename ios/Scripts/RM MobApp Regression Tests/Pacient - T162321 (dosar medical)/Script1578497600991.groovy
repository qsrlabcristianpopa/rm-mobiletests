import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.MobileElement
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils


Mobile.startApplication(GlobalVariable.G_Path, false)

Mobile.tap(findTestObject('RM/T162321/XCUIElementTypeButton - Dosar'), 0)

IOSUtils.scrollDown(IOSUtils.XCUIElementTypeTable)

//Mobile.scrollToText('RM/T162321/XCUIElementTypeStaticText - Informatiile disponibile in aceasta aplicatie sunt extrase din baza de date REGINA MARIA si sunt  citeste mai mult', FailureHandling.STOP_ON_FAILURE)

//Mobile.tap(findTestObject('RM/T162321/XCUIElementTypeStaticText - Informatiile disponibile in aceasta aplicatie sunt extrase din baza de date REGINA MARIA si sunt  citeste mai mult'),0)

//TestObject moreInfo= findTestObject('RM/T162321/XCUIElementTypeStaticText - Informatiile disponibile in aceasta aplicatie sunt extrase din baza de date REGINA MARIA si sunt  citeste mai mult')

MobileElement element = IOSUtils.findElementById("Informatiile disponibile in acesta aplicatie sunt extrase din baza de date REGINA MARIA si sunt  citeste mai mult");
if(element!=null){
	println("Coordinates:"+element.getLocation());
	//Mobile.tapAtPosition(element.getLocation().getX()+element.getSize().getWidth()-10, element.getLocation().getY()+element.getSize().getHeight()-10)
	println("size width:"+element.getSize().getWidth()+" height:"+element.getSize().getHeight());
	IOSUtils.tapAtPosition(element.getLocation().getX()+element.getSize().getWidth()-50, element.getLocation().getY()+10)
//	IOSUtils.scrollDown(IOSUtils.XCUIElementTypeTable)
}





//Mobile.closeApplication()

