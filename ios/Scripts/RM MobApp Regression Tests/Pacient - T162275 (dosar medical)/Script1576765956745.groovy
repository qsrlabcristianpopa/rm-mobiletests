import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - Dosar'), 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - financiarBigIcon'), 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - plus circle'), 0)

Mobile.setText(findTestObject('RM/T162275/XCUIElementTypeTextField - greutate'), '65', 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - Adauga'), 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - editButton'), 0)

//Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeImage - large 5'), 0)

//Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - Continua'), 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - editButton (1)'), 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - Continua'), 0)

//Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeImage - large 4'), 0)

//Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - Continua'), 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - plus circle (1)'), 0)

Mobile.setText(findTestObject('RM/T162275/XCUIElementTypeTextField - alergie'), 'piper', 0)

Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - Adauga'), 0)

Mobile.delay(3)

//Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeStaticText - piper'), 0)

//Mobile.tap(findTestObject('RM/T162275/XCUIElementTypeButton - Sterge'), 0)

Mobile.closeApplication()

