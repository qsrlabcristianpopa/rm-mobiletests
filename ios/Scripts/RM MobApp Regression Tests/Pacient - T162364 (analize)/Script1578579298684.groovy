import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.MobileElement
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils;

/**
 * 
 * @author Cristian Popa
 *  - controalele de date pickers nu contin id-uri
 */

Mobile.startApplication(GlobalVariable.G_Path, false)

Mobile.tap(findTestObject('RM/T162364/XCUIElementTypeButton - Dosar'), 0)

Mobile.tap(findTestObject('RM/T162364/XCUIElementTypeButton - analizeBigIcon'), 0)

Thread.sleep(4000);

MobileElement element = IOSUtils.findElementById("arrow down");
if( element!=null ){
	println("calendar:"+element);
//	element.click();
	IOSUtils.tap(element);
} else {
	throw new StepFailedException("item arrow down doesnt exist");
}

element = IOSUtils.findElementByXpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]");

if(element !=null ){
	IOSUtils.tap(element);
}

element = IOSUtils.findElementById("OK");
if( element!=null ){
	IOSUtils.tap(element);
}


element = IOSUtils.findElementByXpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]");

if(element !=null ){
	IOSUtils.tap(element);
}

element = IOSUtils.findElementById("OK");
if( element!=null ){
	IOSUtils.tap(element);
}

//element = IOSUtils.findElementById("Cauta");
//if( element!=null ){
//	IOSUtils.tap(element);
//}
//



//ArrayList<MobileElement> dates = IOSUtils.findElementsById("miniCalendarIcon");
//if( dates!=null ){
//	println("calendar dates:"+dates.size());
//	for(MobileElement item: dates){
//		IOSUtils.tap(item);
//		break;
//	}
//}



//Mobile.tap(findTestObject('Object Repository/RM/T162364/XCUIElementTypeButtonData'), 0)

//Mobile.tap(findTestObject('Object Repository/RM/T162364/XCUIElementTypeTextField'), 0)
//
//Mobile.tap(findTestObject('RM/T162364/XCUIElementTypeButton - OK'), 0)
//
//Mobile.tap(findTestObject('Object Repository/RM/T162364/XCUIElementTypeTextField'), 0)
//
//Mobile.tap(findTestObject('RM/T162364/XCUIElementTypeStaticText - OK2'), 0)
//
Mobile.tap(findTestObject('RM/T162364/XCUIElementTypeButton - Cauta'), 0)

element = IOSUtils.findElementById("Continua");
if( element!=null ){
	IOSUtils.tap(element);
}

//Mobile.closeApplication()
