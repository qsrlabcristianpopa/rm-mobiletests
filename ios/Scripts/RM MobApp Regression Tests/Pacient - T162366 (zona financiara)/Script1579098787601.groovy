import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - Dosar'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - facturiSiPlatiIcon'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - PLATESTE'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - PLATESTE (1)'), 0)

Mobile.setText(findTestObject('RM/T162366/XCUIElementTypeTextField - ultimele 3 cifre de pe verso card'), '999', 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - Done'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeOther - Luna'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - Next'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - Next'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - Done'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - autorizeaz'), 0)

Mobile.tap(findTestObject('RM/T162366/XCUIElementTypeButton - OK'), 0)

Mobile.closeApplication()

