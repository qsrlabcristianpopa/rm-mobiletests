import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.ios.IOSDriver as IOSDriver
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

IOSUtils.scrollDown(IOSUtils.XCUIElementTypeCollectionView);

//Mobile.scrollToText('dictionar medical', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeButton - dictionar medical'), 0)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeStaticText - Categorie investigatie'), 0)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeStaticText - Genetica'), 0)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeStaticText - C'), 0)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeButton - Item'), 0)

Mobile.setText(findTestObject('RM/T162263/XCUIElementTypeSearchField - Cauta'), 'biolab', 0)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeStaticText - Cariotip lichid amniotic  Fish cromozomii 13 18 21 X i Y'), 
    0)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeStaticText - Indicatii'), 0)

Mobile.tap(findTestObject('RM/T162263/XCUIElementTypeStaticText - Interpretare'), 0)

Mobile.closeApplication()

