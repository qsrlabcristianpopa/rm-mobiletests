import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Locatii'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Home (2)'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Medici'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - backIcon'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Banca de km'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - backIcon'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Calculator BMI'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Nu acum (1)'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Home (2)'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Profilul meu'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - backIcon'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Card de membru'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Home (2)'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Suport'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - backIcon'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeStaticText - Statistici'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Nu'), 0)

Mobile.tap(findTestObject('RM/sample/XCUIElementTypeButton - Item'), 0)

Mobile.closeApplication()

