import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.ios.IOSDriver as IOSDriver
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeButton - Intreaba medic'), 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeTextField'), 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeStaticText - Bucuresti'), 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeTextField (1)'), 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeStaticText - Dermatovenerologie'), 0)

Mobile.setText(findTestObject('RM/T162260/XCUIElementTypeTextField (2)'), '25', 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeButton - Toolbar Done Button'), 0)

Mobile.setText(findTestObject('RM/T162260/XCUIElementTypeTextField (3)'), 'intrebare test', 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeButton - Toolbar Done Button'), 0)

Mobile.setText(findTestObject('RM/T162260/XCUIElementTypeTextView'), 'detalii de test', 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeButton - Toolbar Done Button'), 0)

IOSUtils.scrollToElement("Trimite")

//Mobile.scrollToText('Trimite', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeButton - Trimite'), 0)

Mobile.tap(findTestObject('RM/T162260/XCUIElementTypeButton - Continua'), 0)

Mobile.closeApplication()

