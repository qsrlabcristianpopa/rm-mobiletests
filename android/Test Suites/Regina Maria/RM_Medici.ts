<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Medici</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5a6ef052-5988-48c6-afe3-dee6687d4f5e</testSuiteGuid>
   <testCaseLink>
      <guid>7d7bc1f4-6739-462f-a486-1ca4b4d3c83b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162247 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08725523-cb72-4ffb-9eb6-4521e9b87a7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162248 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a468e1e0-bc4d-4f45-bcf8-98c426973c5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162267 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5679c737-2d3c-497d-af66-50065d19259e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162276 (medici)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
