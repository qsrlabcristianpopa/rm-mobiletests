<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Pacient</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7ce682b2-eaf9-43bc-aba2-34c75ec7ce46</testSuiteGuid>
   <testCaseLink>
      <guid>a672d4eb-5429-4650-8366-018be167d1d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162243 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5614b21b-cd68-4985-9b32-dab8b3f99b2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162244 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6978730-0eb5-468e-92f4-b454e54bef4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162245 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efe28352-0e3e-4ddd-aa73-994030c3e074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162296 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02518844-9912-4a6f-bfe3-e01b7691cbac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162329 (pacient)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
