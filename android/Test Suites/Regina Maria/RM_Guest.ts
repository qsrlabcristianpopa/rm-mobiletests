<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Guest</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>77c87ef1-3972-4cfb-894d-bde9d4510df7</testSuiteGuid>
   <testCaseLink>
      <guid>9b1e80b2-7b09-40db-9710-bf252888e70b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162264 (guest)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a99ace2-5d81-4477-b4c5-54c919db39c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162281 (guest)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffdc7c80-bc20-44fd-a8db-34ff11db63a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162324 (guest)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
