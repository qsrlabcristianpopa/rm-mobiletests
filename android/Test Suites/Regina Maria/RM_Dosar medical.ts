<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Dosar medical</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6ad5972d-8e58-4224-a7a8-ba8fbdf77074</testSuiteGuid>
   <testCaseLink>
      <guid>d32ca7af-203a-4ac8-b7b2-7ac9cd0a0019</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162261 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34129c2d-1bc6-4e21-95f8-cff438d755e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162262 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bdf75f2-a080-4ae9-8475-8f1c144e22e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162274 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45545249-90fe-406f-ae55-ffc74b9d0f1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162275 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2304aac7-8b84-4b4b-909e-523b790cfed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162321 (dosar medical)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
