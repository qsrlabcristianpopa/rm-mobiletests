<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Intreaba un medic</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0ff14842-fc1c-40eb-bea9-dae2bcf87721</testSuiteGuid>
   <testCaseLink>
      <guid>ccb73d57-8a17-43ba-be4d-79889ae3c0ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162260 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f5ea7f2-7f20-407f-959f-5cf1dcd70545</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162327 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3f87fe5-2207-41f3-910b-5f1fe2a0d80e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162328 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5665a931-8ddf-4670-b42f-e871346155b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162354 (intreaba un medic)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
