<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Dasboard</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cfedb51f-398e-43c9-b2b2-85da4a566ab3</testSuiteGuid>
   <testCaseLink>
      <guid>4d667687-d812-478f-908c-b7eed853cd9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162255 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88fe4dbb-7d0f-45ff-b8dd-f87a1d6b72c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162279 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a6062c7-8c62-416a-adab-a8a055a87d6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162286 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23835ccc-8d9f-49ef-a75b-036c3327cacd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162288 (dashboard)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
