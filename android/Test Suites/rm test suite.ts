<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>rm test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fc1e7b69-9a01-4b7f-b6ff-e4b942573990</testSuiteGuid>
   <testCaseLink>
      <guid>7347d80b-746e-416d-97c6-3079546ad3a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162310 (medicii mei)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>076188d9-39d5-4a8f-a5c6-10640ffb3bf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162243 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61181936-55cc-49c9-bcf5-e4e3826686e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162296 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7dc1c54-517c-450e-9713-8305bd6186b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162244 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b9c9f5d-920f-4edc-9e78-cb32d5ce84c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162245 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36f68356-4f33-420e-9294-98c898aac087</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162329 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3d99c1e-23f7-42ce-af03-7cc69879faf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162252 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f804935-8e7d-4755-a016-184ecb61a99b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162253 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0080898-7e22-4676-84ea-f3716d44ff22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162254 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b8351f9-b901-4a30-b47c-581fd18af43d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162322 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3af23a5-c366-4f33-8d46-1d53bc0a6667</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162273 (statistici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c4a05ad-bc55-4e07-84e9-ea4bcb86725e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162361 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd30c80f-0ddb-4d9a-b2b3-2bb733962183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162362 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1449d90-6228-47ea-b70b-7fb7ed7d6957</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162363 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed2c27a2-7423-4f24-902b-def1e5bca5a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162366 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebbd7b52-57ff-4f56-a075-b5213184e66e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162371 (zona financiara)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
