<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM FULL Suite RUN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0b88182e-75b2-42f0-8eaf-e5a2ca04f38f</testSuiteGuid>
   <testCaseLink>
      <guid>dd5fde71-95b5-44f8-9524-ecf087e0127b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162297 (analize)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ad71884-03c7-4f6b-b655-41789458d2b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162364 (analize)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23144d43-99da-44d2-a180-3273f38c73be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162249 (banca de kilometri)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1c6b47d-b99a-4319-b70c-d8cabd2646f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162269 (banca de kilometri)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b932491b-1abf-48ab-a450-597d49fbabb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162250 (banca de kilometri)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>146753ca-f03d-4e83-b5b6-a220811da96a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162293 (banca de kilometri)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bd6623c-2d59-4926-85c5-755ea9721588</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162251 (BMI)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c7743d4-dc23-4552-85eb-912f2c20936d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162325 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b257ac0b-169b-41c3-8889-aeb2bdde7dc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162277 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4be2889a-8bd6-4608-bf18-0ca529e08e21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162303 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d54eb3bf-f4d9-4963-9c18-de502f10a17c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162353 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f04e72b6-38e0-49d2-b180-dc135b458d0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162304 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62c80697-e747-431a-86a6-17f500352dd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162357 (cere o programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52b1ca77-0ed6-48bf-b330-b56106617f51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162305 (creaza cont)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25714b20-ba2f-4ade-abbc-da2c319cec36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162306 (creaza cont)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07481391-5703-46a3-b747-584861574892</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162307 (creaza cont)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>783feafd-2bb1-46ed-b4b4-06ed210617d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162255 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1aac8f9-5703-4330-95c2-7bdabe31830b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162286 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4df4d9a7-2b58-4cbe-b767-d3f51e3feb7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162279 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e9307b6-5f12-4533-a14b-7a100d0075b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162288 (dashboard)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70a288d6-cf76-453e-94d1-25dcab64bbca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162301 (despre)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3347cdc-cb9a-4569-b254-3f48c520334e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162331 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bca1fa0-a6f6-4d23-949d-796e6aa3e060</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162333 (detalii programare</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73b56895-a901-4766-953e-7dbe24e2e7c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162336 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b964694-eede-4b35-aeac-6d8687637167</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162337 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80a02e31-01b8-4169-bd0f-6d2df4042348</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162355 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41bcdffc-3849-4997-a25a-d18bf0ff608a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162359 (detalii programare)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aff42181-af87-409d-8d5c-5d840150156a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162263 (dictionar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6f13c7e-bb56-4c2a-b5a6-faf98599c537</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162265 (dictionar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3de8978b-821e-4fe1-90dd-251bbbf59692</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162261 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4dba6a8a-dcb7-4ed3-a09a-ff53f8547d9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162274 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ec221e9-3895-4917-a361-8b6875248ceb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162275 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70ad97e3-bec8-4fa6-ad54-d87edb093bc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162262 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7126f58e-ec6f-43d4-a381-e0f2b41dc4f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162321 (dosar medical)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62014a96-3459-44cb-bd82-8fa92ca9a6c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162256 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dddb692a-2c5c-41dc-b520-540491310e23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162257 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2608e91c-ea73-4117-bdb1-ff90d915f144</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162258 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d9f3815-a964-403a-9bef-f98186142ea7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162259 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da4bec2a-40f0-45b6-bbd7-729272504f84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162264 (guest)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c571eff0-54ab-4673-b79f-8da5b71b3358</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162281 (guest)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>814c73ab-5f0d-4fe4-968e-4ebe5e6a1df3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162324 (guest)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de0bd790-f864-4999-a58e-2b5d44e3dd6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162246 (Hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dae06a1e-18de-42c2-beb5-9b13d15cbd9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162300 (Hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a256b109-e5f4-4170-9a30-f1b8656cf0ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162320 (hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45454d2e-fd71-402f-b14d-3b6d2477538f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162358 (Hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>398b0923-b4c1-47df-af4f-2fb688856d32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162260 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45985a5e-2a66-49aa-a135-7dcc7354b73e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162327 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f1e43c7-e62b-4045-98d8-7c3f59a13489</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162328 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ecb6451-4f45-4239-b3b6-221000e94440</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162354 (intreaba un medic)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34af0c5c-fd8a-4cac-b120-6da18d111045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162283 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11d2fcaa-09de-48b6-89b6-25cef35eea72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162284 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>deedcef0-948e-4b88-a0ad-e37a3c968150</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162285 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5479571-28cd-4431-a9e7-2f70ab1db131</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162242 (login)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f41938ab-f5b4-4285-a870-bf9f0e2b14a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162247 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c39297f-872f-4237-ba16-e7a4d4a73003</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162248 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9451db68-b862-4f99-b32a-c179a4d88608</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162267 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4465b24f-eb28-4cbd-8490-8a39c28fcad6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162276 (medici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86a30384-82c9-4278-8b76-5dac03770634</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162308 (medicii mei)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20ef4e29-49b7-4325-978c-cc9ac984be24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162309 (medicii mei)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22ba47ae-3837-4c53-95ef-3cb84a721e9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162310 (medicii mei)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a3c317f-750d-409f-b1fe-f81f88c96c77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162243 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cd83b36-18aa-4642-b0da-3b9a7673da98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162296 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c33a9e19-cb9b-4990-abc9-452902bfa7fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162244 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e217215d-8074-4364-8579-1ec42cddaad1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162245 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41b970d9-91f9-470a-8729-0084fd85d3f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162329 (pacient)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62434586-d745-49a3-93ca-5659bc10657d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162252 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6b83f06-0d16-4445-a908-e463feaf7974</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162253 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d22bbc2-288b-4ead-a6fd-4de6deed5a16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162254 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bafbdbc3-57f3-4843-84d8-82ff43c961b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162322 (profil user)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e039629c-7fb1-4686-9f69-4c3db0438f24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162273 (statistici)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c6ba14d-12e7-474e-a445-cf30f743e0d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162361 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7e35a9f-ccc7-4e56-84a0-2cdfca441926</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162362 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7735ec4e-103e-4885-8641-546d08453133</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162363 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04891a8f-77a1-4da8-b9e7-d54392e29b54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162366 (zona financiara)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c356b74-5449-45a8-9d2e-7b583b2b3047</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - T162371 (zona financiara)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
