import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.AppPath, true)

Mobile.tap(findTestObject('Application/RM/login/butonpacient'), 0)

Mobile.setText(findTestObject('Application/RM/T162243/android.widget.EditText0 (4)'), 'catalin.it86@', 0)

Mobile.setText(findTestObject('Application/RM/T162243/android.widget.EditText0 (5)'), '123456.Abc', 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Button0 - Intra in cont (2)'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Button0 - Continua'), 0)

Mobile.setText(findTestObject('Application/RM/login/Email'), GlobalVariable.userName, 0)

Mobile.setText(findTestObject('Application/RM/T162243/android.widget.EditText0 (5)'), '1234', 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Button0 - Intra in cont (2)'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Button0 - Continua'), 0)

Mobile.setText(findTestObject('Application/RM/login/Parola'), GlobalVariable.password, 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Button0 - Intra in cont (2)'), 0)

Mobile.delay(4)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.ImageButton0 (6)'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.ImageButton0 (8)'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Button0 - Iesi din cont'), 0)

Mobile.tap(findTestObject('Application/RM/login/butonpacient'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Switch0 - Pornit'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Switch0 - Oprit'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.TextView0 - Creeaza cont'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.ImageButton0 (9)'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.TextView0 - Reseteaza parola'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.ImageButton0 (10)'), 0)

Mobile.tap(findTestObject('Application/RM/T162243/android.widget.Button0 - Intra in cont (2)'), 0)

Mobile.closeApplication()

