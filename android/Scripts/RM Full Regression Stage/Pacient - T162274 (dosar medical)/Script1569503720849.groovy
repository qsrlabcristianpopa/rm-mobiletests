import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.AppPath, false)

//Mobile.tap(findTestObject('Application/RM/login/butonpacient'), 0)

//Mobile.setText(findTestObject('Application/RM/login/Email'), GlobalVariable.userName, 0)

//Mobile.setText(findTestObject('Application/RM/login/Parola'), GlobalVariable.password, 0)

//Mobile.tap(findTestObject('Application/RM/login/Intra in cont'), 0)

//Mobile.tap(findTestObject('Application/RM/login/afterloginsplash'), 0)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.TextView0 - Dosar'), 0)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.TextView0 - Investigatii si preturi'), 0)

Mobile.delay(8)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.TextView0 - Filtreaza dupa'), 0)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.TextView0 - Locatie'), 0)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.TextView0 - Bucuresti Business Park'), 0)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.TextView0 - SpecialitateCompetente'), 0)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.TextView0 - Medicina generala'), 0)

Mobile.tap(findTestObject('Application/RM/T162274/android.widget.ImageButton0 (2)'), 0)

Mobile.closeApplication()
