import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.AppPath, true)

Mobile.tap(findTestObject('Application/RM/login/butonpacient'), 0)

Mobile.tap(findTestObject('Application/RM/T162305/android.widget.TextView0 - Creeaza cont'), 0)

Mobile.setText(findTestObject('Application/RM/T162305/android.widget.EditText0 (1)'), 'test', 0)

Mobile.tap(findTestObject('Application/RM/T162305/android.widget.Button0 - Urmatorul pas'), 0)

Mobile.setText(findTestObject('Application/RM/T162305/android.widget.EditText0'), 'test@gmail.com', 0)

Mobile.setText(findTestObject('Application/RM/T162305/android.widget.EditText0 - Confirmare email (1)'), 'test@gmail.com',
	0)

Mobile.setText(findTestObject('Application/RM/T162305/android.widget.EditText0 - Parola'), '1234.Abc', 0)

Mobile.setText(findTestObject('Application/RM/T162305/android.widget.EditText0 - Confirmare parola'), '1234.Abc', 0)

Mobile.tap(findTestObject('Application/RM/T162305/android.widget.Button0 - Urmatorul pas'), 0)

Mobile.tap(findTestObject('Application/RM/T162305/android.widget.ImageButton0 (1)'), 0)

Mobile.tap(findTestObject('Application/RM/T162305/android.widget.ImageButton0 (1)'), 0)

Mobile.closeApplication()
